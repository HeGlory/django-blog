"""blogs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import include, url
from mainapp.views import *
from user import views as user_views

urlpatterns = [
    path('', homepage, name='home'),
    path('admin/', admin.site.urls),
    path('new_articles/', new_articles),  # 最新文章
    path('add_article/', add_article),
    path('hot_articles/', hot_articles),  # 热门文章
    re_path(r'^category/(\w?)/$', article_category),
    url(r'^article/(\d+)/$', show_article),  # 文章具体内容
    re_path(r'^archives/(?P<year>\d{4})/(?P<month>\d{1,2})/$', archives),
    path('videos/<str:slug>/', listvideos, name='listvideos-url'),
    path('video/<str:link>/', video),
    path('video/<str:link>/<str:index>', video, name='video-url'),
    path('up/<str:link>/<str:index>', up),  # 视频上一集
    path('down/<str:link>/<str:index>', down),  # 视频下一集
    re_path(r'^kinds/(\d+)/$', kind, name='kind-url'),
    re_path(r'^select/(\d+)/(\d+)$', select),        # 视频选集
    path('search/', search_article),     # 搜索文章
    re_path(r'^blog/$', user_views.index),
    re_path(r'^article/edit/$', user_views.article_edit),
    re_path(r'^blog_article_content/(\d+)/$', user_views.blog_article_content),
    re_path(r'^contract/$', contract),
    re_path(r'^blog_article_kind/(\d+)/$',
            user_views.blog_article_kind, name='blog_article_kind_url'),
    re_path(r'^signin/$',user_views.signin),
    re_path(r'^signup/$',user_views.signup),
    re_path(r'^logout/$',user_views.user_logout),
    re_path(r'^captcha/',include('captcha.urls')),
    re_path(r'^my_blog/$',user_views.my_blog,name='my_blog-url'),
    re_path(r'^article/delete/(\d+)/$',user_views.delete_article),
    re_path(r'^article/edit/(\d+)/$',user_views.update_article),
]
