from django.db import models
from django.db.models.base import ModelState
from django.db.models.fields import CharField, TextField
from django.contrib.auth.models import AbstractUser
# Create your models here.


class Tag(models.Model):
    tagname = models.CharField(max_length=32)

    def __str__(self):
        return self.tagname


class Kind(models.Model):
    kindname = models.CharField(max_length=20)

    def __str__(self):
        return self.kindname


class Article(models.Model):
    CATEGORY_TYPE = (
        ('O', '原创'),
        ('R', '转载')
    )

    PERMISSION_TYPE = (
        ('public', '公开'),
        ('private', '私密'),
        ('fans', '仅粉丝可见')
    )

    author=models.ForeignKey('User',on_delete=models.CASCADE,verbose_name='作者')
    article_id = models.CharField(max_length=20, null=True)
    title = CharField(max_length=200)
    content = models.TextField()
    post_datetime = models.DateTimeField(auto_now_add=True)
    category = models.CharField(
        default='O', choices=CATEGORY_TYPE, max_length=1)
    permission = models.CharField(
        default='public', choices=PERMISSION_TYPE, max_length=8)
    tag = models.ForeignKey(to='Tag', on_delete=models.CASCADE)
    kind = models.ForeignKey(to='Kind', on_delete=models.CASCADE)
    read_count = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.title

class User(AbstractUser):
    nickname=models.CharField(max_length=32,verbose_name='昵称')

    def __str__(self):
        return self.username
