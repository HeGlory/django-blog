from django import forms
from django.db.models import fields
from .models import Article
from django.core.exceptions import ValidationError
from captcha.fields import CaptchaField

class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        exclude = ('read_count','article_id')

class SigninForm(forms.Form):
    username=forms.CharField(label='用户名')
    password=forms.CharField(label='密码')
    captcha=CaptchaField(label='验证码')

class SignupForm(forms.Form):
    username=forms.CharField(label='用户名')
    nickname=forms.CharField(label='昵称')
    password1=forms.CharField(label='密码',widget=forms.PasswordInput)
    password2=forms.CharField(label='确认密码',widget=forms.PasswordInput)

    def clean(self):
        password1=self.cleaned_data.get('password1')
        password2=self.cleaned_data.get('password2')
        if password1==password2:
            password=password2
            del self.cleaned_data['password1']
            del self.cleaned_data['password2']
            self.cleaned_data['password']=password
            return self.cleaned_data
        raise ValidationError('密码输入不一致')

class UpdateArticleForm(forms.ModelForm):
    class Meta:
        model=Article
        exclude=('author','article_id','read_count')
        
