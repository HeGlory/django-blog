from django.contrib import admin
from django.contrib.admin.sites import site
from django.db import models
from .models import Tag, Kind, Article,User
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _


# Register your models here.


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('tagname',)


@admin.register(Kind)
class KindAdmin(admin.ModelAdmin):
    list_display = ('kindname',)


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title','article_id', 'post_datetime',
                    'read_count', 'kind', 'tag','username')
    exclude = ('read_count',)  # 后台管理界面中无法编辑此字段
    
    def username(self,obj):
        return obj.author.username
    username.short_description='用户'


@admin.register(User)
class MyUserAdmin(UserAdmin):
    list_display=('username','nickname','email','last_login')
    fieldsets=(
        (None,{'fields':('username','password')}),
        (_('Personal info'),{'fields':('email','nickname')}),
        (_('Permissions'),{'fields':('is_active',),}),
        (_('Important dates'),{'fields':('last_login','date_joined')}),
    )