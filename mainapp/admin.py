from django.contrib import admin

# Register your models here.
from . import models


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'article_id', 'post_datetime', 'read_count')
    search_fields = ('title',)
    ordering = ('-read_count',)


admin.site.register(models.Article, ArticleAdmin)
admin.site.register(models.Videos)
admin.site.register(models.Kind)
admin.site.register(models.VideosKinds)
