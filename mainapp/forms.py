from django import forms
from django.db import models
from django.db.models import fields
from .models import Article, Videos


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['article_id', 'title', 'content', 'kind']

    def __init__(self, *args, **kwargs):
        super(ArticleForm, self).__init__(*args, **kwargs)
        self.fields['article_id'].label = '文章ID'
        self.fields['title'].label = '文章标题'
        self.fields['content'].label = '文章内容'
        self.fields['kind'].label = '文章类别'


class ContractForm(forms.Form):
    username = forms.CharField(label='您的称呼', max_length=50, initial='匿名')
    email = forms.EmailField(label='电子邮件')
    message = forms.CharField(label='内容', widget=forms.Textarea)
