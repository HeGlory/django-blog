from django.db import models

# Create your models here.www


class Kind(models.Model):
    kindname = models.CharField(max_length=20)

    def __str__(self):
        return self.kindname


class Article(models.Model):
    CATEGORY_TYPE = (
        ('O', '原创'),
        ('R', '转载')
    )
    article_id = models.CharField(max_length=20, verbose_name='文章ID')
    title = models.CharField(max_length=200, verbose_name='主题')
    content = models.TextField(verbose_name='内容')
    post_datetime = models.DateTimeField(
        auto_now_add=True, verbose_name='建立时间')
    category = models.CharField(
        default='O', choices=CATEGORY_TYPE, max_length=1, verbose_name='文章类型')
    read_count = models.PositiveIntegerField(default=0, verbose_name='阅读次数')
    kind = models.ForeignKey(
        to=Kind, on_delete=models.CASCADE, null=True, verbose_name='文章分类')

    class Meta:
        ordering = ('-post_datetime',)

    def __str__(self):
        return self.title


class VideosKinds(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Videos(models.Model):
    kindname = models.ForeignKey(
        to=VideosKinds, on_delete=models.CASCADE, null=True, verbose_name='类别')
    name = models.CharField(max_length=100, verbose_name='名称')
    link = models.CharField(max_length=200, verbose_name='链接')
    count_video = models.PositiveIntegerField(default=0, verbose_name='集数')

    def __str__(self):
        return self.name
